#pragma once

#include <cstdint>

class Siwitch
{
    public:

        Siwitch() = default;
        ~Siwitch() = default;
        void run (const uint32_t sample_count);
        void connectPort (const uint32_t port, void* data);

    private: 
        float* input_a_l;
        float* input_a_r;
        float* input_b_l;
        float* input_b_r;
        float* output_l;
        float* output_r;
        float* input_select;
};
