#include "lv2/core/lv2.h"
#include <stdlib.h>
#include <iostream>

#include "mkl-siwitch.h"
#include "mkl-siwitch.peg"

#define SIWITCH_URI "http://punkt-k.de/plugins/mkl-siwitch"

void Siwitch::connectPort( const uint32_t port, void* data )
{
  switch( port )
  {
    case mkl_siwitch_inselect:
      input_select = (float*)data;
      break;
    case mkl_siwitch_in_a_l:
      input_a_l = (float*)data;
      break;
    case mkl_siwitch_in_a_r:
      input_a_r = (float*)data;
      break;
    case mkl_siwitch_in_b_l:
      input_b_l = (float*)data;
      break;
    case mkl_siwitch_in_b_r:
      input_b_r = (float*)data;
      break;
    case mkl_siwitch_out_l:
      output_l = (float*)data;
      break;
    case mkl_siwitch_out_r:
      output_r = (float*)data;
      break;
  }
}

void Siwitch::run (const uint32_t sample_count) 
{
  for (uint32_t pos = 0; pos < sample_count; pos++)
  {
    const int inports = static_cast< int >( *input_select );

    if( inports == 1 ){
        output_l[pos] = input_a_l[pos];
        output_r[pos] = input_a_r[pos];
    }
    else if( inports  == 2 ){
        output_l[pos] = input_b_l[pos];
        output_r[pos] = input_b_r[pos];
    }
    else if( inports  == 3 ){
        output_l[pos] = input_a_l[pos] + input_b_l[pos];
        output_r[pos] = input_a_r[pos] + input_b_r[pos];
    }
  }
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{

    Siwitch* siwitch = nullptr;
    try 
    {
        siwitch = new Siwitch ();
    } 
    catch (const std::bad_alloc& ba)
    {
        std::cerr << "Failed to allocate memory. Can't instantiate MKL-Siwitch" << std::endl;
        return nullptr;
    }
    return siwitch;
}

// LV2 Methods

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
  Siwitch* siwitch = static_cast< Siwitch* >( instance );
  if( siwitch != nullptr) 
    siwitch->connectPort( port, data );
}


static void
activate(LV2_Handle instance)
{}


static void
run(LV2_Handle instance, uint32_t n_samples)
{
  Siwitch* siwitch = static_cast< Siwitch* >( instance );
  if( siwitch != nullptr) 
    siwitch->run( n_samples );

}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  Siwitch* siwitch = static_cast< Siwitch* >( instance );
  if( siwitch != nullptr) 
    delete siwitch;
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {SIWITCH_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data };

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}