BUNDLE = mkl-siwitch.lv2
INSTALL_DIR = /usr/local/lib/lv2


$(BUNDLE): manifest.ttl mkl-siwitch.ttl src/mkl-siwitch.so
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp manifest.ttl mkl-siwitch.ttl src/mkl-siwitch.so $(BUNDLE)

src/mkl-siwitch.so: src/mkl-siwitch.cpp src/mkl-siwitch.peg
	g++ -shared -fPIC -DPIC src/mkl-siwitch.cpp `pkg-config --cflags --libs lv2-plugin` -o src/mkl-siwitch.so

src/mkl-siwitch.peg: mkl-siwitch.ttl
	lv2peg mkl-siwitch.ttl src/mkl-siwitch.peg

install: $(BUNDLE)
	if [ -d "/usr/lib/lv2" ]; then echo "Dir INSTALL_DIR=/usr/lib exists"; INSTALL_DIR=/usr/lib/lv2; fi
	if [ -d "/usr/local/lib/lv2" ]; then echo "Dir /usr/local/lib exists"; INSTALL_DIR=/usr/local/lib/lv2; fi
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) src/mkl-siwitch.so rc/mkl-siwitch.peg
